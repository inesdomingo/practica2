package mvc;



import com.github.lgooddatepicker.components.DatePicker;
import com.inesd.hibernate.*;

import javax.swing.*;

public class Vista {
    JTabbedPane tabbedPane1;
    private JPanel panel1;
    JFrame frame;
    //regalo
    JTextField tipoTxt;
    JRadioButton espanyolRB;
    JRadioButton inglesRB;
    JList<Regalo> listregalos;
    JTextField precioTxtRe;
    JCheckBox nombreCB;
    JCheckBox fechaCB;
    JButton eliminarRegaloBtn;
    JButton altaRegaloBtn;
    JButton modificarRegaloBtn;
    //cliente
    JTextField nombreTxt;
    JTextField apellidoTxt;
    JTextField CiudadTxt;
    DatePicker fecha_Cliente;
    JCheckBox pedido;
    JButton modificarClienteBtn;
    JButton altaClienteBtn;
    JButton eliminarClienteBtn;
    JList<Cliente> listClientes;

    //pedidos
    JComboBox RegaloCB;
    JComboBox ClienteCB;
    JTextField preciotxt;
    DatePicker fecha_pedido;
    JList listPedidos;
    JCheckBox internet;
    JCheckBox pagado;
    JButton altaPedidoBtn;
    JButton modificarPedidoBtn;
    JButton eliminarPedidoBtn;
    JList listEmpleadosTienda;
    JTextField nombreEmpleado;
    JTextField edadEmpleadoTxt;
    JComboBox TiendaCB;
    DatePicker fecha_empleado;
    JList listEmpleados;
    JComboBox EmpleadoCB;

    JButton altaEmpleadoBtn;
    JButton modificarEmpleadoBtn;
    JButton eliminarEmpleadoBtn;
     //tienda
     JTextField nombreTienda;
     DatePicker fecha_apertura;
     JCheckBox abierto;
    JTextField telefono_tiendaTxt;
    JTextField ciudad_tiendaTxt;
    JList listTienda;
    JButton altaTiendaBtn;
    JButton eliminarTiendaBtn;
    JButton modificarTiendBtn;
    JButton mostrarET;


    DefaultListModel<Regalo> dlmRegalo;
    DefaultListModel<Cliente> dlmCliente;
    DefaultListModel<Pedido> dlmPedido;
    DefaultListModel<Empleado> dlmEmpleado;
    DefaultListModel<Tienda> dlmTienda;
    DefaultListModel<Empleado>dlmEmpleadoTienda;
    JRadioButtonMenuItem mysqlItem;
    JRadioButtonMenuItem salirItem;
    JRadioButtonMenuItem actualizarItem;


    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        iniciarModelos();
        iniciarMenu();
        frame.pack();
        frame.setLocationRelativeTo(null);

    }

    private void iniciarModelos() {
        dlmRegalo = new DefaultListModel<Regalo>();
        dlmCliente = new DefaultListModel<Cliente>();
        dlmPedido = new DefaultListModel<Pedido>();
        dlmEmpleado = new DefaultListModel<Empleado>();
        dlmTienda = new DefaultListModel<Tienda>();
        dlmEmpleadoTienda= new DefaultListModel<Empleado>();
        listClientes.setModel(dlmCliente);
        listregalos.setModel(dlmRegalo);
        listPedidos.setModel(dlmPedido);
        listEmpleados.setModel(dlmEmpleado);
        listTienda.setModel(dlmTienda);
        listEmpleadosTienda.setModel(dlmEmpleadoTienda);
    }

    private void iniciarMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Conexion");
        mysqlItem = new JRadioButtonMenuItem("Conectar a MySql");
        mysqlItem.setSelected(true);
        mysqlItem.setActionCommand("mysql");
        menu.add(mysqlItem);
        salirItem= new JRadioButtonMenuItem("Desconectar");
        salirItem.setSelected(true);
        salirItem.setActionCommand("desconectar");
        menu.add(salirItem);
        actualizarItem = new JRadioButtonMenuItem("Actualizar");
        actualizarItem.setActionCommand("Actualizar");
        menu.add(actualizarItem);



        barra.add(menu);
        frame.setJMenuBar(barra);

        ButtonGroup grupoConexion = new ButtonGroup();
        grupoConexion.add(mysqlItem);
    }

}
