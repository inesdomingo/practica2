package mvc;

import com.inesd.hibernate.*;
import net.bytebuddy.asm.Advice;

import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Date;
import java.util.ArrayList;


public class Controlador implements ActionListener, ListSelectionListener, WindowListener{
    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        anadirActionListeners(this);
        anadirListSeletionListeners(this);
        anadirWindowListeners(this);

        //En el momento en que haga la vista visible,
        // se dispara el evento WindowEvent, que es capturado por el listener WindowOpened
        vista.frame.setVisible(true);
    }

    /**
     * Vinculo un listener con los elementos graficos
     * @param listener el listener
     */
    private void anadirActionListeners(ActionListener listener){
        vista.altaClienteBtn.addActionListener(listener);
        vista.altaRegaloBtn.addActionListener(listener);
        vista.altaPedidoBtn.addActionListener(listener);
        vista.altaEmpleadoBtn.addActionListener(listener);
        vista.altaTiendaBtn.addActionListener(listener);

        vista.eliminarClienteBtn.addActionListener(listener);
        vista.eliminarRegaloBtn.addActionListener(listener);
        vista.eliminarPedidoBtn.addActionListener(listener);
        vista.eliminarEmpleadoBtn.addActionListener(listener);
        vista.eliminarTiendaBtn.addActionListener(listener);

        vista.modificarClienteBtn.addActionListener(listener);
        vista.modificarRegaloBtn.addActionListener(listener);
        vista.modificarPedidoBtn.addActionListener(listener);
        vista.modificarTiendBtn.addActionListener(listener);
        vista.modificarEmpleadoBtn.addActionListener(listener);
        vista.mysqlItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.actualizarItem.addActionListener(listener);

        vista.mostrarET.addActionListener(listener);

    }

    /**
     * Vinculo un listener con los elementos graficos
     * @param listener el listener
     */
    private void anadirListSeletionListeners(ListSelectionListener listener){
        vista.listregalos.addListSelectionListener(listener);
        vista.listClientes.addListSelectionListener(listener);
        vista.listPedidos.addListSelectionListener(listener);
        vista.listEmpleados.addListSelectionListener(listener);
        vista.listTienda.addListSelectionListener(listener);
        vista.listEmpleadosTienda.addListSelectionListener(listener);

    }

    /**
     * Vinculo un listener con los elementos graficos
     * @param listener el listener
     */
    public void anadirWindowListeners(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }

    /**
     * Vinculo un listemer con los elementos graficos
     */
    private void anadirChangeListeners(ChangeListener listener){

    }

    /**
     * Método que responde a un evento ActionEvent sobre los botones
     * @param e el evento lanzado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch(comando){
            case "AltaCliente":{
                modelo.altaCliente(vista.nombreTxt.getText(),vista.apellidoTxt.getText(),Date.valueOf(vista.fecha_Cliente.getDateStringOrEmptyString()),vista.CiudadTxt.getText(),vista.pedido.isSelected());
                break;
            }
            case "ModificarCliente":{
                if(!vista.listClientes.isSelectionEmpty()){
                    Cliente cliente = vista.listClientes.getSelectedValue();
                    modificarCliente(cliente);
                    modelo.modificarCliente(cliente);


                }else{
                    System.out.println("selecciona un cliente");
                }
            }
            break;
            case "EliminarCliente":{
                if(!vista.listClientes.isSelectionEmpty()) {
                    Cliente cliente = vista.listClientes.getSelectedValue();
                    vista.dlmCliente.removeElement(cliente);
                    modelo.eliminarCliente(cliente);

                }else{
                    System.out.println("selecciona un cliente");
                }
            }
            break;
            case "AltaRegalo":{
                if(vista.espanyolRB.isSelected()) {
                    modelo.altaRegalo(vista.tipoTxt.getText(), Float.parseFloat(vista.precioTxtRe.getText()), vista.nombreCB.isSelected(), vista.fechaCB.isSelected(),"espanyol");
                }
                else if(vista.inglesRB.isSelected()){
                    modelo.altaRegalo(vista.tipoTxt.getText(), Float.parseFloat(vista.precioTxtRe.getText()), vista.nombreCB.isSelected(), vista.fechaCB.isSelected(),"ingles" );

                }
                break;
            }

            case "ModificarRegalo":{
                if(!vista.listregalos.isSelectionEmpty()){
                    Regalo regalo = vista.listregalos.getSelectedValue();
                    modificarRegalo(regalo);
                    modelo.modificarRegalo(regalo);


                }else{
                    System.out.println("selecciona un regalo");
                }
            }
            break;
            case "EliminarRegalo":{
                if(!vista.listregalos.isSelectionEmpty()) {
                    Regalo regalo = vista.listregalos.getSelectedValue();
                    vista.dlmRegalo.removeElement(regalo);
                    modelo.eliminarRegalo(regalo);

                }else{
                    System.out.println("selecciona un regalo");
                }
            }
            break;
            case "altaEmpleado":{
                modelo.altaEmpleado(vista.nombreEmpleado.getText(),Integer.parseInt(vista.edadEmpleadoTxt.getText()),Date.valueOf(String.valueOf(vista.fecha_empleado)),(Tienda)vista.TiendaCB.getItemAt(vista.TiendaCB.getSelectedIndex()));
                break;
            }

            case "modificarEmpleado":{
                if(!vista.listEmpleados.isSelectionEmpty()){
                    Empleado empleado = (Empleado) vista.listEmpleados.getSelectedValue();
                    modificarEmpleado(empleado);
                    modelo.modificarEmpleado(empleado);


                }else{
                    System.out.println("selecciona un empleado");
                }
            }
            break;
            case "EliminarEmpleado":{
                if(!vista.listEmpleados.isSelectionEmpty()) {
                    Empleado empleado = (Empleado) vista.listEmpleados.getSelectedValue();
                    vista.dlmEmpleado.removeElement(empleado);
                    modelo.eliminarEmpleado(empleado);

                }else{
                    System.out.println("selecciona un empleado");
                }
            }
            break;
            case "AltaPedido":{
                modelo.altaPedido(Float.parseFloat(vista.preciotxt.getText()),vista.internet.isSelected(),vista.pagado.isSelected(),Date.valueOf(vista.fecha_pedido.getDateStringOrEmptyString()),(Cliente)vista.ClienteCB.getItemAt(vista.ClienteCB.getSelectedIndex()),(Regalo)vista.RegaloCB.getItemAt(vista.RegaloCB.getSelectedIndex()),(Empleado)vista.EmpleadoCB.getItemAt(vista.EmpleadoCB.getSelectedIndex()));
                break;
            }

            case "ModificarPedido":{
                if(!vista.listPedidos.isSelectionEmpty()){
                    Pedido pedido  = (Pedido) vista.listPedidos.getSelectedValue();
                    modificarPedido(pedido);
                    modelo.modificarPedido(pedido);


                }else{
                    System.out.println("selecciona un pedido");
                }
            }
            break;
            case "EliminarPedido":{
                if(!vista.listPedidos.isSelectionEmpty()) {
                    Pedido pedido = (Pedido) vista.listPedidos.getSelectedValue();
                    vista.dlmPedido.removeElement(pedido);
                    modelo.eliminarPedido(pedido);

                }else{
                    System.out.println("selecciona un pedido");
                }
            }
            break;
            case "AltaTienda":{
                modelo.altaTienda(vista.nombreTienda.getText(),Date.valueOf(vista.fecha_apertura.getDateStringOrEmptyString()),vista.abierto.isSelected(),vista.telefono_tiendaTxt.getText(), vista.ciudad_tiendaTxt.getText());
                break;
            }

            case "ModificarTienda":{
                if(!vista.listregalos.isSelectionEmpty()){
                    Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
                    modificarTienda(tienda);
                    modelo.modificarTienda(tienda);


                }else{
                    System.out.println("selecciona una tienda");
                }
            }
            break;
            case "EliminarTienda":{
                if(!vista.listTienda.isSelectionEmpty()) {
                    Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
                    vista.dlmTienda.removeElement(tienda);
                    modelo.eliminarTienda(tienda);

                }else{
                    System.out.println("selecciona un tienda");
                }
            }
            break;
            case "Empleados por tienda":{
               Tienda tiendanueva= (Tienda) vista.listTienda.getSelectedValue();
                tiendanueva.setEmpleados((ArrayList) modelo.getEmpleadosTienda(tiendanueva));
                listarEmpleadosTienda(modelo.getEmpleadosTienda(tiendanueva));
            }break;


            case "mysql":{
                modelo.desconectar();
                //Me conecto a mysql
                modelo.conectar(true);
            }
            break;
            case "desconectar":{
                modelo.desconectar();

            }

            break;
            case "Actualizar":{
               listarClienteComboBox(modelo.getCliente());
               listarCliente();
               listarEmpleadosTienda(modelo.getEmpleado());
               listarEmpleado();
               listarRegalo();
               listarRegaloComboBox(modelo.getRegalos());
               listarPedido();
               listarTienda();
               listarTiendaComboBox(modelo.getTienda());
            }break;


        }
        listarRegalo();
        listarTienda();
        listarPedido();
        listarEmpleado();
        listarCliente();


    }


    public void modificarRegalo(Regalo regalo) {
        regalo.setTipoRegalo(vista.tipoTxt.getText());
        regalo.setPrecio(Float.valueOf(vista.precioTxtRe.getText()));
        regalo.setEscritoNombre(vista.nombreCB.isSelected());
        regalo.setEscritoFecha(vista.fechaCB.isSelected());
        if(vista.inglesRB.isSelected()){
            regalo.setIdioma("ingles");
        }
        else if(vista.espanyolRB.isSelected()){
            regalo.setIdioma("espanyol");
        }
    }

    /**
     * Modifico al cliente a partir de los datos de los campos del cliente
     * @param cliente el cliente seleccionado
     */
    public void modificarCliente(Cliente cliente){
       cliente.setNombre(vista.nombreTxt.getText());
       cliente.setApellido(vista.apellidoTxt.getText());
       cliente.setFechaNacimiento(Date.valueOf(vista.fecha_Cliente.toString()));
       cliente.setCiudad(vista.CiudadTxt.getText());
       cliente.setPedido(vista.pedido.isSelected());
    }
    /**
     * Modifico el pedido a partir de los datos de los campos del pedido
     * @param pedido el pedido seleccionado
     */
    public void modificarPedido(Pedido pedido){
        pedido.setCliente((Cliente)vista.ClienteCB.getItemAt(vista.ClienteCB.getSelectedIndex()));
        pedido.setEmpleado((Empleado) vista.EmpleadoCB.getItemAt(vista.EmpleadoCB.getSelectedIndex()));
        pedido.setFechaPedido(Date.valueOf(vista.fecha_pedido.toString()));
        pedido.setInternet(vista.internet.isSelected());
        pedido.setPagado(vista.pagado.isSelected());
        pedido.setPrecio(Float.parseFloat(vista.preciotxt.getText()));
        pedido.setRegalo((Regalo) vista.RegaloCB.getItemAt(vista.RegaloCB.getSelectedIndex()));


    }
    public void modificarEmpleado(Empleado empleado){
        empleado.setEdad(Integer.parseInt(vista.edadEmpleadoTxt.getText()));
        empleado.setNombre(vista.nombreEmpleado.getText());
        empleado.setFechaInicio(Date.valueOf(vista.fecha_empleado.toString()));
        empleado.setTienda((Tienda)vista.TiendaCB.getItemAt(vista.TiendaCB.getSelectedIndex()));
    }

    public void modificarTienda(Tienda tienda){
        tienda.setAbierto(vista.abierto.isSelected());
        tienda.setCiudad(vista.ciudad_tiendaTxt.getText());
        tienda.setFechaApertura(Date.valueOf(vista.fecha_apertura.toString()));
        tienda.setTelefono(vista.telefono_tiendaTxt.getText());
        tienda.setNombre(vista.nombreTienda.getText());
    }
    /**
     * Listo las regalos
     */
    private void listarRegalo(){
        vista.dlmRegalo.clear();
        ArrayList<Regalo> lista = modelo.getRegalos();
        for(Regalo regalo: lista){
            vista.dlmRegalo.addElement(regalo);
        }
    }
    private void listarTienda(){
        vista.dlmTienda.clear();
        ArrayList<Tienda> lista = modelo.getTienda();
        for(Tienda tienda : lista){
            vista.dlmTienda.addElement(tienda);
        }
    }
    private void listarPedido(){
        vista.dlmPedido.clear();
        ArrayList<Pedido> lista = modelo.getPedidos();
        for(Pedido pedido: lista){
            vista.dlmPedido.addElement(pedido);
        }
    }
    private void listarCliente(){
        vista.dlmCliente.clear();
        ArrayList<Cliente> lista = modelo.getCliente();
        for(Cliente cliente: lista){
            vista.dlmCliente.addElement(cliente);
        }
    }
    private void listarEmpleado(){
        vista.dlmEmpleado.clear();
        ArrayList<Empleado> lista = modelo.getEmpleado();
        for(Empleado empleado: lista){
            vista.dlmEmpleado.addElement(empleado);
        }
    }
    /**
     * listo los empleados que trabajan en una tienda
     */
    private void listarEmpleadosTienda(ArrayList<Empleado> empleadosLocal) {
        vista.dlmEmpleadoTienda.clear();
        for(Empleado soc : empleadosLocal) {
            vista.dlmEmpleadoTienda.addElement(soc);
        }}
    /**
     * Listo las tiendas en el ComboBox de tiendas
     */
    private void listarTiendaComboBox(ArrayList<Tienda> lista){
        vista.dlmTienda.clear();
        for(Tienda tienda : lista){
            vista.dlmTienda.addElement(tienda);
        }

        vista.TiendaCB.removeAllItems();
        ArrayList<Tienda> ins = modelo.getTienda();

        for (Tienda in : ins){
            vista.TiendaCB.addItem(in);
        }
        vista.TiendaCB.setSelectedIndex(-1);

    }
    /**
     * Listo los regalo en el ComboBox de regalo
     */
    private void listarRegaloComboBox(ArrayList<Regalo> lista){
        vista.dlmRegalo.clear();
        for(Regalo regalo : lista){
            vista.dlmRegalo.addElement(regalo);
        }

        vista.RegaloCB.removeAllItems();
        ArrayList<Regalo> ins = modelo.getRegalos();

        for (Regalo regalo : ins){
            vista.RegaloCB.addItem(regalo);

        }
        vista.RegaloCB.setSelectedIndex(-1);

    }
    /**
     * Listo los EMPLEADOS en el ComboBox de EMPLEADOS
     */
    private void listarEmpleadoComboBox(ArrayList<Empleado> lista){
        vista.dlmEmpleado.clear();
        for(Empleado empleado : lista){
            vista.dlmEmpleado.addElement(empleado);
        }

        vista.EmpleadoCB.removeAllItems();
        ArrayList<Empleado> ins = modelo.getEmpleado();

        for (Empleado empleado : ins){
            vista.EmpleadoCB.addItem(empleado);

        }
        vista.EmpleadoCB.setSelectedIndex(-1);

    }
    /**
     * Listo los regalos en el ComboBox de regalo
     */
    private void listarClienteComboBox(ArrayList<Cliente> lista){
        vista.dlmCliente.clear();
        for(Cliente cliente : lista){
            vista.dlmCliente.addElement(cliente);
        }

        vista.ClienteCB.removeAllItems();
        ArrayList<Cliente> ins = modelo.getCliente();

        for (Cliente  cliente : ins){
            vista.ClienteCB.addItem(cliente);

        }
        vista.ClienteCB.setSelectedIndex(-1);

    }



    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listregalos) {
                mostrarDatosRegalo();
            } else if (e.getSource() == vista.listClientes) {
                mostrarDatosCliente();
            } else if (e.getSource() == vista.listPedidos) {
                mostrarDatosPedido();
            }else if (e.getSource() == vista.listEmpleados) {
            mostrarDatosEmpleado();
            }else if (e.getSource() == vista.listTienda) {
                mostrarDatosTienda();
            } else if (e.getSource() == vista.listEmpleadosTienda) {
                seleccionarEmpleadoTienda();
            }
        }

    }


    private void seleccionarEmpleadoTienda() {
        if(!vista.listEmpleadosTienda.isSelectionEmpty()){
            Tienda tienda = (Tienda) vista.listEmpleadosTienda.getSelectedValue();
            vista.tabbedPane1.setSelectedIndex(0);
            vista.listEmpleados.setSelectedValue(tienda, true);
           mostrarDatosEmpleado();
        }
    }
    /**
     * Metodo que me muestra los datos en los campos de texto, del pedido seleccionado en el JList
     */
    private void mostrarDatosPedido() {
        if(!vista.listPedidos.isSelectionEmpty()){
            Pedido pedido =(Pedido) vista.listPedidos.getSelectedValue();
           vista.preciotxt.setText(String.valueOf(pedido.getPrecio()));
           vista.internet.setSelected(pedido.getInternet());
           vista.pagado.setSelected(pedido.getPagado());
           listarRegaloComboBox(modelo.getRegalos());
           listarClienteComboBox(modelo.getCliente());
           listarEmpleadoComboBox(modelo.getEmpleado());
           vista.RegaloCB.setSelectedItem(pedido.getRegalo());
           vista.ClienteCB.setSelectedItem(pedido.getCliente());
           vista.EmpleadoCB.setSelectedItem(pedido.getEmpleado());
        }
    }

    /**
     * Metodo que me muestra los datos en los campos de texto, del empleado seleccionado en el JList
     */
    private void mostrarDatosEmpleado() {
        if(!vista.listEmpleados.isSelectionEmpty()){
           Empleado empleado =(Empleado) vista.listEmpleados.getSelectedValue();
           vista.nombreEmpleado.setText(empleado.getNombre());
           vista.fecha_empleado.setDate(empleado.getFechaInicio().toLocalDate());
           vista.edadEmpleadoTxt.setText(String.valueOf(empleado.getEdad()));
            listarTiendaComboBox(modelo.getTienda());
            vista.TiendaCB.setSelectedItem(empleado.getTienda());
        }
    }
    /**
     * Metodo que me muestra los datos en los campos de texto, de la tienda seleccionado en el JList
     */
    private void mostrarDatosTienda() {
        if(!vista.listTienda.isSelectionEmpty()){
            Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
            vista.nombreTienda.setText(tienda.getNombre());
            vista.fecha_apertura.setDate(tienda.getFechaApertura().toLocalDate());
            vista.ciudad_tiendaTxt.setText(tienda.getCiudad());
            vista.telefono_tiendaTxt.setText(tienda.getTelefono());
            vista.abierto.setSelected(tienda.getAbierto());
            //listarVictimasHomicida();
        }
    }
    /**
     * Metodo que me muestra los datos en los campos de texto, del cliente seleccionado en el JList
     */
    public  void mostrarDatosCliente() {
        if (!vista.listClientes.isSelectionEmpty()) {
            Cliente cliente = (Cliente) vista.listClientes.getSelectedValue();
            vista.nombreTxt.setText(cliente.getNombre());
            vista.fecha_Cliente.setDate(cliente.getFechaNacimiento().toLocalDate());
            vista.apellidoTxt.setText(cliente.getApellido());
            vista.CiudadTxt.setText(cliente.getCiudad());
            vista.pedido.setSelected(cliente.getPedido());
            listarTiendaComboBox(modelo.getTienda());

        }
    }

    /**
     * Metodo que me muestra los datos en los campos de texto, del regalo seleccionada en el JList
     */
    private void mostrarDatosRegalo(){
        if(!vista.listregalos.isSelectionEmpty()){
           Regalo regalo = vista.listregalos.getSelectedValue();
            vista.tipoTxt.setText(regalo.getTipoRegalo());
            vista.espanyolRB.setSelected(regalo.getIdioma().equalsIgnoreCase("espanyol"));
            vista.inglesRB.setSelected(regalo.getIdioma().equalsIgnoreCase("ingles"));
            vista.preciotxt.setText(String.valueOf(regalo.getPrecio()));
            vista.nombreCB.setSelected(regalo.getEscritoNombre());
            vista.fechaCB.setSelected(regalo.getEscritoFecha());


        }
    }


    /**
     * Cuando se abre la vista, conecto hibernate y listo los diferentes elementos
     * @param e
     */
    @Override
    public void windowOpened(WindowEvent e) {
        //Le indico que estoy conectando con mysql
        modelo.conectar(true);
       listarCliente();
       listarEmpleado();
       listarPedido();
       listarRegalo();
       listarTienda();
       listarTiendaComboBox(modelo.getTienda());
       listarRegaloComboBox(modelo.getRegalos());
       listarEmpleadoComboBox(modelo.getEmpleado());
       listarClienteComboBox(modelo.getCliente());

    }

    /**
     * Cuando se cierra la vista, desconecto hibernate
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        modelo.desconectar();
    }


    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


}
