package mvc;

import com.inesd.hibernate.*;
import com.mysql.fabric.xmlrpc.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.persistence.Query;
import javax.swing.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Modelo {

     SessionFactory factoria;
    public Session sesion;

    public void conectar(boolean mysql){
        Configuration configuracion = new Configuration();

        configuracion.configure("hibernate.cfg.xml");
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Regalo.class);
        configuracion.addAnnotatedClass(Pedido.class);
        configuracion.addAnnotatedClass(Tienda.class);
        configuracion.addAnnotatedClass(Empleado.class);
        configuracion.addAnnotatedClass(Seccion.class);


        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuracion.getProperties()).build();
        factoria = configuracion.buildSessionFactory(serviceRegistry);

        //Creo una sesion a partir de la factoria
        sesion=factoria.openSession();

    }

    public void desconectar(){
        if(sesion != null){
            sesion.close();
        }
        if(factoria != null){
            factoria.close();
        }
    }
    public void altaCliente(String nombre, String apellido, Date fecha_nacimiento, String ciudad, boolean pedido ){

        Cliente cliente=new Cliente(nombre,apellido, fecha_nacimiento,ciudad,pedido);
        sesion.beginTransaction();
        sesion.save(cliente);
        sesion.getTransaction().commit();
    }

    public void modificarCliente(Cliente cliente){

        sesion.beginTransaction();
        sesion.saveOrUpdate(cliente);
        sesion.getTransaction().commit();
    }

    public void eliminarCliente(Cliente cliente){

        sesion.beginTransaction();
        sesion.delete(cliente);
        sesion.getTransaction().commit();
    }
    public void altaRegalo(String tipo_regalo,float precio,boolean escrito_nombre,boolean escrito_fecha,String idioma){

        Regalo regalo=new Regalo(tipo_regalo,precio,escrito_nombre,escrito_fecha,idioma);
        sesion.beginTransaction();
        sesion.save(regalo);
        sesion.getTransaction().commit();
    }

    public void modificarRegalo(Regalo regalo){

        sesion.beginTransaction();
        sesion.update(regalo);
        sesion.getTransaction().commit();
    }

    public void eliminarRegalo(Regalo regalo){

        sesion.beginTransaction();
        //homicida.removeAllVictimas();
        sesion.delete(regalo);
        sesion.getTransaction().commit();
    }
    public void altaPedido(float precio, boolean internet, boolean pagado, Date fecha_nacimiento, Cliente cliente, Regalo regalo, Empleado empleado){

        Pedido pedido=new Pedido(precio,internet,pagado,fecha_nacimiento, regalo, cliente,empleado);
        sesion.beginTransaction();
        sesion.save(pedido);
        sesion.getTransaction().commit();
    }

    public void modificarPedido(Pedido pedido){

        sesion.beginTransaction();
        sesion.update(pedido);
        sesion.getTransaction().commit();
    }

    public void eliminarPedido(Pedido pedido){

        sesion.beginTransaction();
        sesion.delete(pedido);
        sesion.getTransaction().commit();
    }
    public void altaEmpleado(String nombre,int edad,Date fechaInicio,Tienda tienda){
        Empleado empleado=new Empleado( nombre,edad, fechaInicio, tienda);
        sesion.beginTransaction();
        sesion.save(empleado);
        sesion.getTransaction().commit();
    }

    public void modificarEmpleado(Empleado empleado){
        sesion.beginTransaction();
        sesion.update(empleado);
        sesion.getTransaction().commit();
    }

    public void eliminarEmpleado(Empleado empleado){

        sesion.beginTransaction();
        //homicida.removeAllVictimas();
        sesion.delete(empleado);
        sesion.getTransaction().commit();
    }
    public void altaTienda(String nombre, Date fechaApertura, boolean abierto,String telefono, String ciudad){

        Tienda tienda=new Tienda( nombre,fechaApertura,abierto,telefono,ciudad);
        sesion.beginTransaction();
        sesion.save(tienda);
        sesion.getTransaction().commit();
    }

    public void modificarTienda(Tienda tienda){
        sesion.beginTransaction();
        sesion.update(tienda);
        sesion.getTransaction().commit();
    }

    public void eliminarTienda(Tienda tienda){

        sesion.beginTransaction();
        //homicida.removeAllVictimas();
        sesion.delete(tienda);
        sesion.getTransaction().commit();
    }


    public ArrayList<Regalo> getRegalos() {
        javax.persistence.Query query = sesion.createQuery("FROM Regalo ");
        ArrayList<Regalo>lista = (ArrayList<Regalo>)query.getResultList();

        return lista;
    }

    public ArrayList<Pedido> getPedidos(){
        javax.persistence.Query query = sesion.createQuery("FROM Pedido ");
        ArrayList<Pedido> lista = (ArrayList<Pedido>)query.getResultList();
        return lista;
    }
    public ArrayList<Cliente> getCliente(){

        javax.persistence.Query query = sesion.createQuery("FROM Cliente ");
        ArrayList<Cliente> lista = (ArrayList<Cliente>)query.getResultList();

        return lista;
    }
    public ArrayList<Empleado> getEmpleado(){

        javax.persistence.Query query = sesion.createQuery("FROM Empleado");
        ArrayList<Empleado> lista = (ArrayList<Empleado>)query.getResultList();

        return lista;
    }
    public ArrayList<Tienda> getTienda(){
        javax.persistence.Query query = sesion.createQuery("FROM Tienda");
        ArrayList<Tienda> lista = (ArrayList<Tienda>)query.getResultList();

        return lista;
    }
        public ArrayList<Empleado> getEmpleadosTienda(Tienda tienda) {
        Query query = sesion.createQuery("FROM Empleado WHERE tienda = :prop");
        query.setParameter("prop", tienda);
        ArrayList<Empleado> lista = (ArrayList<Empleado>) query.getResultList();
        return lista;
    }

}
