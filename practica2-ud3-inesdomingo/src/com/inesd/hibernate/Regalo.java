package com.inesd.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "regalo", schema = "tienda_regalos")
public class Regalo {
    private int idRegalo;
    private String tipoRegalo;
    private float precio;
    private boolean escritoNombre;
    private boolean escritoFecha;
    private String idioma;
    private List<Pedido> pedidos;
    private List<Tienda> tiendas;

   public Regalo(String tipoRegalo,float precio,boolean escritoFecha,boolean escritoNombre,String idioma){
       this.tipoRegalo=tipoRegalo;
       this.precio=precio;
       this.escritoFecha=escritoFecha;
       this.escritoNombre=escritoNombre;
       this.idioma=idioma;
   }

    public Regalo() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_regalo")
    public int getIdRegalo() {
        return idRegalo;
    }

    public void setIdRegalo(int idRegalo) {
        this.idRegalo = idRegalo;
    }

    @Basic
    @Column(name = "tipo_regalo")
    public String getTipoRegalo() {
        return tipoRegalo;
    }

    public void setTipoRegalo(String tipoRegalo) {
        this.tipoRegalo = tipoRegalo;
    }

    @Basic
    @Column(name = "precio")
    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "escrito_nombre")
    public boolean getEscritoNombre() {
        return escritoNombre;
    }

    public void setEscritoNombre(boolean escritoNombre) {
        this.escritoNombre = escritoNombre;
    }

    @Basic
    @Column(name = "escrito_fecha")
    public boolean getEscritoFecha() {
        return escritoFecha;
    }

    public void setEscritoFecha(boolean escritoFecha) {
        this.escritoFecha = escritoFecha;
    }

    @Basic
    @Column(name = "idioma")
    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Regalo regalo = (Regalo) o;
        return idRegalo == regalo.idRegalo &&
                Double.compare(regalo.precio, precio) == 0 &&
                escritoNombre == regalo.escritoNombre &&
                escritoFecha == regalo.escritoFecha &&
                Objects.equals(tipoRegalo, regalo.tipoRegalo) &&
                Objects.equals(idioma, regalo.idioma);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRegalo, tipoRegalo, precio, escritoNombre, escritoFecha, idioma);
    }

    @OneToMany(mappedBy = "regalo",cascade = CascadeType.ALL)

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "regalo_tienda", schema = "tienda_regalos", joinColumns = @JoinColumn(name = "id_tienda", referencedColumnName = "id_regalo", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_regalo", referencedColumnName = "id_tienda", nullable = false))
    public List<Tienda> getTiendas() {
        return tiendas;
    }

    public void setTiendas(List<Tienda> tiendas) {
        this.tiendas = tiendas;
    }

    @Override
    public String toString() {
        return "Regalo{" +
                "idRegalo=" + idRegalo +
                ", tipoRegalo='" + tipoRegalo +
                ", precio=" + precio +
                ", escritoNombre=" + escritoNombre +
                ", escritoFecha=" + escritoFecha +
                ", idioma='" + idioma;
    }
}
