package com.inesd.hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "pedido", schema = "tienda_regalos")
public class Pedido {
    private int idPedido;
    private float precio;
    private boolean internet;
    private boolean pagado;
    private Date fechaPedido;
    private Regalo regalo;
    private Cliente Cliente;
    private Empleado Empleado;

    public Pedido(float precio, boolean internet,boolean pagado, Date fechaPedido,Regalo regalo,Cliente cliente,Empleado empleado){
        this.precio=precio;
        this.internet=internet;
        this.pagado=pagado;
        this.fechaPedido=fechaPedido;
        this.regalo=regalo;
        this.Cliente=cliente;
        this.Empleado=empleado;
    }

    public Pedido() {

    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pedido")
    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    @Basic
    @Column(name = "precio")
    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "internet")
    public boolean getInternet() {
        return internet;
    }

    public void setInternet(boolean internet) {
        this.internet = internet;
    }

    @Basic
    @Column(name = "pagado")
    public boolean getPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    @Basic
    @Column(name = "fecha_pedido")
    public Date getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pedido pedido = (Pedido) o;
        return idPedido == pedido.idPedido &&
                Double.compare(pedido.precio, precio) == 0 &&
                internet == pedido.internet &&
                pagado == pedido.pagado &&
                Objects.equals(fechaPedido, pedido.fechaPedido);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPedido, precio, internet, pagado, fechaPedido);
    }

    @ManyToOne
    @JoinColumn(name = "id_regalo", referencedColumnName = "id_regalo", nullable = false)
    public Regalo getRegalo() {
        return regalo;
    }

    public void setRegalo(Regalo regalo) {
        this.regalo = regalo;
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", nullable = false)
    public com.inesd.hibernate.Cliente getCliente() {
        return Cliente;
    }

    public void setCliente(com.inesd.hibernate.Cliente cliente) {
        Cliente = cliente;
    }

    @ManyToOne
    @JoinColumn(name = "id_empleado", referencedColumnName = "id_empleado", nullable = false)
    public com.inesd.hibernate.Empleado getEmpleado() {
        return Empleado;
    }

    public void setEmpleado(com.inesd.hibernate.Empleado empleado) {
        Empleado = empleado;
    }

    @Override
    public String toString() {
        return
                "precio=" + precio +
                ", internet=" + internet +
                ", pagado=" + pagado +
                ", fechaPedido=" + fechaPedido +
                ", regalo=" + regalo.getIdRegalo() +
                ", Cliente=" + Cliente.getIdCliente()+
                ", Empleado=" + Empleado.getIdEmpleado();
    }
}
