package com.inesd.hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "tienda", schema = "tienda_regalos")
public class Tienda {
    private int idTienda;
    private String nombre;
    private Date fechaApertura;
    private boolean abierto;
    private String telefono;
    private String ciudad;
    private List<Regalo> regalos;
    private List<Empleado> empleados;
    public Tienda(String nombre, Date fechaApertura, boolean abierto,String telefono, String ciudad){
        this.nombre=nombre;
        this.fechaApertura=fechaApertura;
        this.abierto=abierto;
        this.telefono=telefono;
        this.ciudad=ciudad;
    }

    public Tienda() {

    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tienda")
    public int getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(int idTienda) {
        this.idTienda = idTienda;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "fecha_apertura")
    public Date getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    @Basic
    @Column(name = "abierto")
    public boolean getAbierto() {
        return abierto;
    }

    public void setAbierto(boolean abierto) {
        this.abierto = abierto;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tienda tienda = (Tienda) o;
        return idTienda == tienda.idTienda &&
                abierto == tienda.abierto &&
                Objects.equals(nombre, tienda.nombre) &&
                Objects.equals(fechaApertura, tienda.fechaApertura) &&
                Objects.equals(telefono, tienda.telefono) &&
                Objects.equals(ciudad, tienda.ciudad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTienda, nombre, fechaApertura, abierto, telefono, ciudad);
    }

    @ManyToMany(cascade = CascadeType.DETACH,mappedBy = "tiendas")
    public List<Regalo> getRegalos() {
        return regalos;
    }

    public void setRegalos(List<Regalo> regalos) {
        this.regalos = regalos;
    }

    @OneToMany(mappedBy = "tienda",cascade = CascadeType.ALL)
    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    @Override
    public String toString() {
        return "" +
                "idTienda=" + idTienda +
                ", nombre='" + nombre + '\'' +
                ", fechaApertura=" + fechaApertura +
                ", abierto=" + abierto +
                ", telefono='" + telefono + '\'' +
                ", ciudad='" + ciudad ;
    }
}
