-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-02-2022 a las 22:31:12
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda_regalos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `ciudad` varchar(25) NOT NULL,
  `pedido` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre`, `apellido`, `fecha_nacimiento`, `ciudad`, `pedido`) VALUES
(1, 'nadia', 'gonzalez', '2022-02-01', 'madrid', 1),
(4, 'ines', 'domingo', '2020-01-08', 'albacete', 0),
(5, 'hola', 'mostrr', '2022-02-18', 'albacete', 0),
(6, 'sara', 'ferrer', '2022-02-17', 'valladolid', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id_empleado` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `edad` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `id_tienda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id_empleado`, `nombre`, `edad`, `fecha_inicio`, `id_tienda`) VALUES
(1, 'PEPO', 55, '2022-02-08', 3),
(2, 'amigo', 55, '2011-02-15', 3),
(3, 'nadii', 53, '2022-02-21', 1),
(4, 'co', 34, '2022-02-21', 2),
(5, 'maria', 45, '2022-02-15', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `id_pedido` int(11) NOT NULL,
  `id_regalo` int(11) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `precio` float NOT NULL,
  `internet` tinyint(1) NOT NULL,
  `pagado` tinyint(1) NOT NULL,
  `fecha_pedido` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id_pedido`, `id_regalo`, `id_empleado`, `id_cliente`, `precio`, `internet`, `pagado`, `fecha_pedido`) VALUES
(2, 2, 2, 1, 557, 1, 0, '2022-02-24'),
(3, 2, 3, 4, 65, 0, 1, '2018-02-13'),
(4, 2, 3, 4, 67, 0, 1, '2018-02-13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regalo`
--

CREATE TABLE `regalo` (
  `id_regalo` int(11) NOT NULL,
  `tipo_regalo` varchar(25) NOT NULL,
  `precio` float NOT NULL,
  `escrito_nombre` tinyint(1) NOT NULL,
  `escrito_fecha` tinyint(1) NOT NULL,
  `idioma` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `regalo`
--

INSERT INTO `regalo` (`id_regalo`, `tipo_regalo`, `precio`, `escrito_nombre`, `escrito_fecha`, `idioma`) VALUES
(2, 'aparato', 4, 0, 1, 'ingles'),
(3, 'movil', 56, 0, 1, 'ingles'),
(4, 'funda', 4, 1, 0, 'ingles'),
(5, 'taza', 44, 1, 0, 'espanyol'),
(6, 'olla', 55, 1, 0, 'ingles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regalo_tienda`
--

CREATE TABLE `regalo_tienda` (
  `id_rtienda` int(11) NOT NULL,
  `id_regalo` int(11) NOT NULL,
  `id_tienda` int(11) NOT NULL,
  `nombre_seccion` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `id_tienda` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `fecha_apertura` date NOT NULL,
  `abierto` tinyint(1) NOT NULL,
  `telefono` varchar(9) NOT NULL,
  `ciudad` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tienda`
--

INSERT INTO `tienda` (`id_tienda`, `nombre`, `fecha_apertura`, `abierto`, `telefono`, `ciudad`) VALUES
(1, 'ines', '2022-02-18', 1, '65432', 'madrid'),
(2, 'Hola ', '2022-02-17', 1, '65432', 'zaragoza'),
(3, 'nose', '2022-02-12', 1, '3565432', 'madrid'),
(4, 'tienda2', '2022-02-18', 0, '6543456', 'madrid');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id_empleado`),
  ADD KEY `id_tienda` (`id_tienda`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_regalo` (`id_regalo`),
  ADD KEY `id_empleado` (`id_empleado`);

--
-- Indices de la tabla `regalo`
--
ALTER TABLE `regalo`
  ADD PRIMARY KEY (`id_regalo`);

--
-- Indices de la tabla `regalo_tienda`
--
ALTER TABLE `regalo_tienda`
  ADD PRIMARY KEY (`id_rtienda`),
  ADD KEY `id_regalo` (`id_regalo`),
  ADD KEY `id_tienda` (`id_tienda`);

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`id_tienda`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id_empleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `regalo`
--
ALTER TABLE `regalo`
  MODIFY `id_regalo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `regalo_tienda`
--
ALTER TABLE `regalo_tienda`
  MODIFY `id_rtienda` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tienda`
--
ALTER TABLE `tienda`
  MODIFY `id_tienda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`id_tienda`) REFERENCES `tienda` (`id_tienda`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`),
  ADD CONSTRAINT `pedido_ibfk_3` FOREIGN KEY (`id_regalo`) REFERENCES `regalo` (`id_regalo`),
  ADD CONSTRAINT `pedido_ibfk_4` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id_empleado`);

--
-- Filtros para la tabla `regalo_tienda`
--
ALTER TABLE `regalo_tienda`
  ADD CONSTRAINT `regalo_tienda_ibfk_1` FOREIGN KEY (`id_regalo`) REFERENCES `regalo` (`id_regalo`),
  ADD CONSTRAINT `regalo_tienda_ibfk_2` FOREIGN KEY (`id_tienda`) REFERENCES `tienda` (`id_tienda`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
